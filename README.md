# How to start the project
This is a single gradle project developed using Spring Boot 2. It also includes a gradle wrapper, so only Java 8+ is required.

Clone the repository and than start it using:

  - ./gradlew bootRun --console=plain (UNIX-like OS)
  - gradlew.bat bootRun --console=plain (Windows)

The embedded Tomcat listens at port 8080 (Spring Boot default).