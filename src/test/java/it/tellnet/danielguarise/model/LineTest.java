/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tellnet.danielguarise.model;

import it.danielguarise.patternrecognition.model.Line;
import it.danielguarise.patternrecognition.model.Point;
import java.util.HashSet;
import java.util.Set;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Daniel Guarise
 */
public class LineTest {

    private Line l;

    @Before
    public void initTest() {
        l = new Line();
    }

    @Test
    public void countPointsInLine() {
        //Line has two points
        l.addPoints(new Point(1.0d, 1.0d), new Point(9.0d, 9.0d));
        Assert.assertEquals(2, l.getPoints().size());

        //Line has three points
        l.addPoints(new Point(5.0d, 5.0d));
        Assert.assertEquals(3, l.getPoints().size());

        //Line has three points (no duplicate points allowed)
        l.addPoints(new Point(5.0d, 5.0d));
        Assert.assertEquals(3, l.getPoints().size());
    }

    @Test
    public void doesNotContainSamePoints() {
        l.addPoints(new Point(1.0d, 1.0d), new Point(5.0d, 5.0d), new Point(9.0d, 9.0d));

        //null check
        Set<Point> otherPoints = null;
        Assert.assertFalse(l.containsSamePoints(otherPoints));

        //one different point check
        otherPoints = new HashSet<>();
        otherPoints.add(new Point(1.0d, 2.0d));
        Assert.assertFalse(l.containsSamePoints(otherPoints));

        //two different point check
        otherPoints.add(new Point(3.0d, 2.0d));
        Assert.assertFalse(l.containsSamePoints(otherPoints));

        //one&one check
        otherPoints = new HashSet<>();
        otherPoints.add(new Point(1.0d, 1.0d));
        otherPoints.add(new Point(3.0d, 2.0d));
        Assert.assertFalse(l.containsSamePoints(otherPoints));
    }

    @Test
    public void containSamePoints() {
        l.addPoints(new Point(1.0d, 1.0d), new Point(5.0d, 5.0d), new Point(9.0d, 9.0d));

        Set<Point> otherPoints = new HashSet<>();

        //one point check
        otherPoints.add(new Point(1.0d, 1.0d));
        Assert.assertTrue(l.containsSamePoints(otherPoints));

        //two points check
        otherPoints.add(new Point(5.0d, 5.0d));
        Assert.assertTrue(l.containsSamePoints(otherPoints));
    }

}
