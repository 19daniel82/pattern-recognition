/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tellnet.danielguarise.model;

import it.danielguarise.patternrecognition.ServerBootstrap;
import it.danielguarise.patternrecognition.model.Point;
import it.danielguarise.patternrecognition.model.Space;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Daniel Guarise
 */
@SpringBootTest(classes = ServerBootstrap.class)
@RunWith(SpringRunner.class)
public class SpaceTest {

    @Autowired
    private Space space;

    @Test
    public void addPointsTest() {
        //Space starts with no points
        space.clear();
        Assert.assertEquals(0, space.getPoints().size());

        //Space has three points
        space.addPoint(new Point(0.0d, 0.0d));
        space.addPoint(new Point(1.0d, 1.0d));
        space.addPoint(new Point(2.0d, 2.0d));
        Assert.assertEquals(3, space.getPoints().size());

        //Space has three points (no duplicate points allowed)
        space.addPoint(new Point(2.0d, 2.0d));
        Assert.assertEquals(3, space.getPoints().size());

        //Space has six points
        space.addPoint(new Point(3.0d, 3.0d));
        space.addPoint(new Point(0.0d, 3.0d));
        space.addPoint(new Point(1.0d, 3.0d));
        Assert.assertEquals(6, space.getPoints().size());
    }

    @Test
    public void getLinesContainingPointsTest() {
        space.clear();
        space.addPoint(new Point(0.0d, 0.0d));
        space.addPoint(new Point(1.0d, 1.0d));
        space.addPoint(new Point(2.0d, 2.0d));
        space.addPoint(new Point(2.0d, 2.0d));
        space.addPoint(new Point(3.0d, 3.0d));
        space.addPoint(new Point(0.0d, 3.0d));
        space.addPoint(new Point(1.0d, 3.0d));
        Assert.assertEquals(8, space.getLinesContainingPoints(2).size());
        Assert.assertEquals(2, space.getLinesContainingPoints(3).size());
        Assert.assertEquals(1, space.getLinesContainingPoints(4).size());
        Assert.assertEquals(0, space.getLinesContainingPoints(5).size());
    }

}
