/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tellnet.danielguarise.model;

import it.danielguarise.patternrecognition.model.Line;
import it.danielguarise.patternrecognition.model.Point;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Daniel Guarise
 */
public class PointTest {

    private Point p;

    @Before
    public void initTest() {
        p = new Point(5.0d, 5.0d);
    }

    @Test
    public void pointBelongsToLine() {
        Line l = new Line();
        l.addPoints(new Point(1.0d, 1.0d), new Point(9.0d, 9.0d));

        Assert.assertTrue(p.belongsToLine(l));
    }

    @Test
    public void pointDoesNotBelongsToLine() {
        Line l = new Line();
        l.addPoints(new Point(1.0d, 1.0d), new Point(9.0d, 8.0d));

        Assert.assertFalse(p.belongsToLine(l));
    }
}
