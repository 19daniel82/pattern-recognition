/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tellnet.danielguarise.service;

import it.danielguarise.patternrecognition.ServerBootstrap;
import it.danielguarise.patternrecognition.model.Point;
import it.danielguarise.patternrecognition.service.PatternRecognitionService;
import java.util.List;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Daniel Guarise
 */
@SpringBootTest(classes = ServerBootstrap.class)
@RunWith(SpringRunner.class)
public class ServiceTest {

    @Autowired
    private PatternRecognitionService service;

    @Test
    public void getLinesContainingPointsTest() {
        service.addPoint(new Point(0.0d, 0.0d));
        service.addPoint(new Point(1.0d, 1.0d));
        service.addPoint(new Point(2.0d, 2.0d));
        service.addPoint(new Point(3.0d, 3.0d));
        service.addPoint(new Point(0.0d, 3.0d));
        service.addPoint(new Point(1.0d, 3.0d));

        List<List<Point>> result = service.getLinesContainingPoints(2);
        for (List<Point> points : result) {
            Assert.assertThat(points.size(), Matchers.greaterThanOrEqualTo(2));
        }

        result = service.getLinesContainingPoints(3);
        for (List<Point> points : result) {
            Assert.assertThat(points.size(), Matchers.greaterThanOrEqualTo(3));
        }
        
        result = service.getLinesContainingPoints(4);
        for (List<Point> points : result) {
            Assert.assertThat(points.size(), Matchers.greaterThanOrEqualTo(4));
        }
    }

}
