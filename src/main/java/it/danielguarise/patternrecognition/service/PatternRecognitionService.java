/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.danielguarise.patternrecognition.service;

import it.danielguarise.patternrecognition.model.Line;
import it.danielguarise.patternrecognition.model.Point;
import it.danielguarise.patternrecognition.model.Space;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Daniel Guarise
 */
@Service
public class PatternRecognitionService {

    @Autowired
    private Space space;

    public void addPoint(final Point p) {
        space.addPoint(p);
    }

    public Set<Point> getSpacePoints() {
        return space.getPoints();
    }

    public List<List<Point>> getLinesContainingPoints(final int n) {
        List<Line> lines = space.getLinesContainingPoints(n);

        //For each matching line gets and collects its points, so they can be serialized to JSON
        return lines.stream().map(Line::getPoints).collect(Collectors.toList());
    }

    public void clearSpace() {
        space.clear();
    }

}
