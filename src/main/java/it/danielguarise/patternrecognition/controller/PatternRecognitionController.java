/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.danielguarise.patternrecognition.controller;

import it.danielguarise.patternrecognition.model.Point;
import it.danielguarise.patternrecognition.service.PatternRecognitionService;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Daniel Guarise
 */
@Slf4j
@RestController
public class PatternRecognitionController {

    @Autowired
    private PatternRecognitionService service;

    @RequestMapping(value = "/point", method = RequestMethod.POST)
    public ResponseEntity<Void> addPoint(@RequestBody final Point req) {
        log.info(String.format("addPoint called {%s}", req));
        service.addPoint(req);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/space", method = RequestMethod.GET)
    public ResponseEntity<Set<Point>> getSpacePoints() {
        log.info("getSpacePoints called");
        return ResponseEntity.ok(service.getSpacePoints());
    }

    @RequestMapping(value = "/lines/{n}", method = RequestMethod.GET)
    public ResponseEntity getLinesContainingPoints(@PathVariable final int n) {
        ResponseEntity response;
        log.info("getLinesContainingPoints called");        
        if (n < 2) {            
            response = ResponseEntity.badRequest().body("N must be equal or greater than 2");
        } else {
            response = ResponseEntity.ok(service.getLinesContainingPoints(n));
        }
        return response;
    }

    @RequestMapping(value = "/space", method = RequestMethod.DELETE)
    public ResponseEntity<Void> clearSpace() {
        log.info("clearSpace called");
        service.clearSpace();
        return ResponseEntity.ok().build();
    }
}
