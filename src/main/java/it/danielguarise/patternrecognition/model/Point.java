/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.danielguarise.patternrecognition.model;

import java.util.ArrayList;
import lombok.Data;

/**
 *
 * @author Daniel Guarise
 */
@Data
public class Point {

    private final double x;
    private final double y;
    
    /**
    * Returns true if the area of the triangle formed by this point and the first two line points is zero.
    * The area of the triangle is calculated using the determinant of a matrix
    *
    * @param line
    * @return true if this point belongs to the specified line, false otherwise
    */
    public boolean belongsToLine(final Line line) {
        final ArrayList<Point> linePoints = line.getPoints();

        final Point p1 = linePoints.get(0);
        final Point p2 = linePoints.get(1);

        return (p1.getX() - p2.getX()) * (p2.getY() - y) - (p2.getX() - x) * (p1.getY() - p2.getY()) == 0d;
    }

}
