/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.danielguarise.patternrecognition.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 *
 * @author Daniel Guarise
 */
@Slf4j
@Component
public class Space {

    //All points in the space
    private final Set<Point> points = new HashSet<>();

    //All lines in the space classified by the their number of points
    private Map<Integer, ArrayList<Line>> linesMap = new HashMap<>();

    public void addPoint(final Point p) {        
        if (!points.contains(p)) { //Ignore duplicates
            addPointToLines(p);
            points.add(p);
        }
    }

    public Set<Point> getPoints() {
        return points;
    }

    /**
    * Collects all values (lines) with a key greater than n 
    * 
    * @param n
    * @return all lines with n or more points
    */
    public List<Line> getLinesContainingPoints(final int n) {
        return linesMap.entrySet().stream().filter(e -> e.getKey() >= n).map(Map.Entry::getValue).flatMap(Collection::stream).collect(Collectors.toList());
    }

    public void clear() {
        points.clear();
        linesMap.clear();
    }

    private void addPointToLines(final Point p) {
        //Checks if the new point belongs to existing lines
        Map<Integer, ArrayList<Line>> newLinesMap = new HashMap<>();
        linesMap.keySet().forEach((key) -> {
            for (Line line : linesMap.get(key)) {
                if (p.belongsToLine(line)) {
                    log.info(String.format("%s belongs to %s", p, line));
                    line.addPoints(p); //Adds the new point to the existing line
                }
                //Builds the local map
                ArrayList<Line> nextKeyLines = newLinesMap.getOrDefault(line.getSize(), new ArrayList<>());
                nextKeyLines.add(line);
                if (nextKeyLines.size() == 1) {
                    newLinesMap.put(line.getSize(), nextKeyLines);
                }
            }
        });
        linesMap = newLinesMap; //Updates the global map

        //Calculates new 2-points lines
        calculateNewLines(p);
    }

    /**
    * Cycles through all points in the space and verifies if they form new 2-points lines with newPoint
    *
    * @param newPoint 
    */
    private void calculateNewLines(final Point newPoint) {
        final Set<Point> twoPoints = new HashSet<>(2);
        twoPoints.add(newPoint);

        ArrayList<Line> twoPointsLines = linesMap.getOrDefault(2, new ArrayList<>());
        for (Point p : points) {
            twoPoints.add(p);
            if (!lineExist(twoPoints)) {
                //New 2-points line detected
                Line newLine = new Line();
                newLine.addPoints(p, newPoint);
                twoPointsLines.add(newLine);
                log.info(String.format("New %s detected", newLine));
            }
            twoPoints.remove(p);
        }

        linesMap.put(2, twoPointsLines); //Updates the global map
    }

    /**
    * Checks if any of the existing lines contains the specified 2-points line
    *
    * @param twoPoints
    * @return true if a line containing twoPoints already exists, false otherwise
    */
    private boolean lineExist(final Set<Point> twoPoints) {
        return linesMap.values().stream().flatMap(Collection::stream).anyMatch(l -> l.containsSamePoints(twoPoints));
    }

}
