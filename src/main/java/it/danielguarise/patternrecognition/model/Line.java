/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.danielguarise.patternrecognition.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import lombok.ToString;

/**
 *
 * @author Daniel Guarise
 */
@ToString
public class Line {

    private final Set<Point> points = new HashSet<>();

    public ArrayList<Point> getPoints() {
        return new ArrayList<>(points);
    }

    public void addPoints(final Point... newPoints) {
        for (Point p : newPoints) {
            points.add(p);
        }
    }

    public boolean containsSamePoints(final Set<Point> otherPoints) {
        return otherPoints != null ? points.containsAll(otherPoints) : false;
    }

    public int getSize() {
        return points.size();
    }

}
