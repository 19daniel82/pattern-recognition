/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.danielguarise.patternrecognition;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author Daniel Guarise
 */
@SpringBootApplication
public class ServerBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(ServerBootstrap.class, args);
    }
}
